# On Smithing

*By Nurat of clan Brassbuckle*

## Contents

*Most of this long scroll is a well-written text on the art of smithing. There are descriptions on training how to create basic work pieces; how to keep your tools clean and how to store them.
Most of the text is targetted at smiths on the level of an apprentice or a journeyman. Amidst these beginner´s lessons however, there are some chapters regarding practices that are not commonly taught elsewhere.*

### On the use of silver

*A text explaining the following facts in great detail*

- Generally, weapons made from pure silver are too soft to hold their blade edge long
- There are some exceptions however, in which making bladed weapons from silver make sense:
	- The most common use for using silvered weapons is to defend against lycanthropes such as werewolves. The relatively high cost of the material often prevents those who are most likely to be attacked by werewolves (villagers and townspeople in rural areas) from affordin these weapons at all.
	- It is rumoured that silvered weapons are capable of wounding the malevolent denizens of the nine hells and the abyss.
	- Sometimes, silver is simply the only metal you have available.
		*Someone has scrawled a note on the side of the scroll: "Not worth it. Make coins from the silver, buy iron."*
- It is possible to use a regular weapon, such as a steel-forged sword and add a coating of silver, to achieve a similar effect without losing the stability of the blade.

### On acid etching

*Several chapters describe techniques to use acid to etch ornamental or runic designs into metal surfaces*

- The smaller the surface, the harder it is to etch meaningful designs into it
- You must use an acid strong enough to attack the metallic surface
	- This means that great care must be taken when handling the acid, to not harm yourself or your tools
- When the design does not have to be seen, such as a runic inscription, it is often beneficial to apply it on the inside of the object, such as a helm or breastplate.
- There are several ways to etch a surface
	- Acid bath (the parts which are not to be etched are covered up)
	- Manually applying the acid (not unlike a painter)
	- Engraving the metal first, and then filling the acid into the engraved parts. 	- The most visually striking and durable way to do this, but also the most arduous, because the acid must be removed without attacking the rest of the work piece

