tags: #flammable #fire-damage #fire #liquid-fire #frostpeak #clan-frostpeak #bagan-dimforge #dwarven-invention #material #steel-barrel #earthen-pot #volatile-material #dangeous-material #barrel #explosion #alchemy #chemical-fire

## Description

This is a barrel containing 300 lb. of *breath of baator* (liquid fire), an *extremely volatile* and *flammable* liquid.
The barrel is sealed with a metal lid.

## Properties

The liquid can be used to create high-temperature fire very quickly. 
It is *sticky*, *floats on water* and is *not extinguished by water*.

Fire or temperatures above 40°C ignite the material in a violent flame, which then turns into a fierce fire which burns out in about a minute.

# Explosion

Should the material ignite within the barrel, it causes an extreme flash of flame, destroying the barrel and spreading the burning liquid over a radius of 30 ft.

The initial explosion causes 1d12 slashing damage and 10d8 fire damage.  
The burning material will stick to surfaces and creatures within the 30 ft. radius and will slowly burn down over a duration of 20 rounds. 
Anyone in contact with the burning liquid takes 1d8 fire damge per round until the liquid has burnt out or they are extinguished in another way.

# Burning

Adding more liquid extends the duration and might spread the fire to more squares, but does not produce the heat flash again.

Once burning it can be put out by magic such as *Control flame* or needs to be suffocated completely and be given time to cool down or it will reignite immediately (dealing the initial damage again but not regaining duration).