tags: #flammable #fire-damage #fire #liquid-fire #frostpeak #clan-frostpeak #bagan-dimforge #dwarven-invention #material #steel-barrel #earthen-pot #volatile-material #dangeous-material

## Description

This is a pot containing 1 lb. of *breath of baator* (liquid fire), an *extremely volatile* and *flammable* liquid.
The pot is sealed with a lid of wax.

## Properties

The liquid can be used to create high-temperature fire very quickly. 
It is *sticky*, *floats on water* and is *not extinguished by water*.

Fire or temperatures above 40°C ignite the material in a violent flame, which then turns into a fierce fire which burns out in about a minute.

## As a weapon

The pot can be thrown like an improvised weapon. 
Use STR or DEX to throw. Maximum range is twice your STR score.

The pot breaks on impact against hard surfaces (e.g. hitting a human and then dropping onto a stone floor.), spilling the *breath of baator* on a 5 ft. square.

It does not ignite by itself, fire or large heat (>40° C) is required.

## Burning

On *ignition*, it deals 3d8 fire damage to everthing in contact with it.
Every turn after that [^1], it deals 1d8 fire damage, until it burns out after 10 turns.
Adding more liquid extends the duration and might spread the fire to more squares, but does not produce the heat flash again.

Once burning it can be put out by magic such as *Control flame* or needs to be suffocated completely and be given time to cool down or it will reignite immediately (dealing the initial damage again but not regaining duration).

[^1:] DM note: For single creatures, on the *end of their turn*. Alternatively, if many creatures are caught in the effect, or if it sticks to surfaces, roll a flat d20 for INI and resolve the effect on that initiative count.