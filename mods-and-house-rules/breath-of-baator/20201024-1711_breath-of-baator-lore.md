tags: #flammable #fire-damage #fire #liquid-fire #frostpeak #clan-frostpeak #bagan-dimforge #dwarven-invention #material #steel-barrel #earthen-pot #volatile-material #dangeous-material #lore #chemical-fire #alchemy

# Material: "Breath of Baator"

## Overview

Invented by Bagan Dimforge of clan *Frostpeak*, Breath of Baator is a very flammable liquid that has seen use as siege weapon and with traps.  
It is created from various rare chemical components mined from the deep mountains and does in fact not need magic in the process.

The liquid itself is very sticky, floats on water and is ignited even by the smallest fire. Even a hot midday in the desert might set it aflame.  

Due to its nature, it is very hard to extinguish, but covering it completely with sand is known to work. Although extinguishing a burning person this way doesn´t work particularly well.

Many scholars have pointed out that the name is misleading as the plane of Baator (colloquially known as "the nine hells") is in fact not covered in flames for the most part.  
But story has it, that Bagan believed this to be the case and choose the name accordingly (and scholars don't argue with dwarfes that posses liquid fire).

## Storage & Production

It can be kept in earthen pots or steel barrels, but will dissolve wood within weeks. 

The process of creating *Breath of Baator* has been stolen from the Frostpeak-Clan some time ago, but is very complicated and expensive due to the needed equipment and resources.


## Sale and transportation

Availability for purchase is very rare and prices vary wildly based on demand.

Part of the excessive prices for this liquid stem from the fact that many travelling merchants, trade caravans and ships flat out refuse to transport this extremely dangerous material.  
In case it is stolen, it means a huge loss of money, but worse, should an accident occur, the chances of survival are slim.

If it is transported, this usually happens in small containers which can be sold individually, instead of large barrels, which typically contain 300lb. of material.
Travel through hot climates is never done, as even the hot midday sun can cause the the liquid to ignite.

## Use

Despite the fact that this is most often used in warfare, some other, non-violent uses have arisen for this material:

- Very small amounts of it can be used to quickly start a bonfire, even with bad materials, such as damp wood or moss. This was the actual intended use for the liquid.
- Quickly generating sufficient heat in a forge. 
