# House rule: Casting spells from spell scrolls

## Quick summary

Anyone can at least *try* to cast a spell from most spell scrolls.  
Make an INT check against **10 plus the spell level**.

Routined spellcasters can do it without making the check.

## Flowchart

![](./images/diagrams/20201204-1313_house-rule-scrollcasting-dnd-v2.png)

<div hidden>
```plantuml
@startUml 20201204-1313_house-rule-scrollcasting-dnd-v2
if (Spell on your spell list) then (yes)
	if (sufficient slot available) then (yes)
		:Automatic success, cast spell without problems;
		stop
	else (no)
		:Make the check;
		stop
	endif
else (no)
	:Make the check;
	stop
endif
@endUml
```
</div>

## Full Rules text

**Anyone** can cast a spell from a scroll by succeeding at an INT ability check.  
The DC is **10 plus the spell level**.

The spell scroll is always *consumed* in the process.

You can skip the check if you fulfill all of the following conditions:

- The spell is on your class spell list 
- You have reached a spell slot level equal to or higher than the spell level.

## Lore

> Spell scrolls are created by binding the spell into the scroll. This is a process that consumes lots of time, rare inks, expensive parchment and the materials components of the spell.  
> The finished product is a scroll which can be used to release the stored magical energy in the form of the spell.
>
> There are a lot of myths regarding the process, some true, others not. Rumours say that the parchment itself is created from the skin of dead wizards, which is of course nothing but a slanderous accusation...