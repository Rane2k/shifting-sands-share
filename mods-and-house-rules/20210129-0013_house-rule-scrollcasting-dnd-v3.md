# House rule: Casting spells from spell scrolls

**Alpha version.** These rules are subject to change as we play with them.

## Quick summary

Anyone can at least *try* to cast a spell from most spell scrolls.  
Make an INT(Arcana) check against the DC, which is always **10 plus twice the spell level**. 
This DC is also used in some failure consequences.

**Its just simple instructions.** Anyone can use their passive INT(Arcana) (10 + (Arcana proficiency) + INT modifier) instead of rolling.

**I already know this.** Any spellcaster who has high enough spell slots and the spell on their class spell list can skip the roll and automatically succeeds.

**I can do this my way.** Any spellcaster can use their spellcasting ability (e.g. CHA, WIS) instead of INT.

A failure does not necessarily mean the spell is not cast.
Check by how much you failed and add a **d12**, then consult the following table.

The GM can rule that you have disadvantage on either the check or the **d12** roll if there is a (previously signalled) complication, such as the scroll is damaged or in a language you are not proficient in.

# Side effect table

1 	Success, Save STR or be knocked prone.  
2	Success. Save INT or fall asleep for 1 minute or until woken up.  
3	Success, Save CON or become *deaf* for 1 minute.  
4	Success, Save CON or become *blinded* for 1 minute.  
5	Success, Save INT or suffer psychic damage: 1d6 per spell level.  
6	Success, Save CON or become *silenced* for 1 minute.  
7	Success, Save WIS or become paralyzed until the end of your next turn.  
8	Success. A very loud thunderclap can be heard within a 1000 ft. radius, you and anyone within 30 ft. save CON or become *deafened* for 10 minutes. Critical failure = also suffer 1d4 thunder damage per spell level.  
9	Success, Save CON or become poisoned until the end of your next turn.  
10	Success, roll on the *Wild magic table*.  
11	Success, You and anyone within 30 ft. save STR or be knocked prone.  
12	Success, save INT or suffer one level of exhaustion.  
13	Success, save INT or you can not concentrate on anything for 1 minute.  
14	Success, but the GM tells you a thematically apropriate side-effect of the spell that happens in addition to all its other effects. Re-roll if no idea.  
15	Success, You and anyone within 15 ft. save DEX or take 1d4 force damage per spell level.  
16	Success, Save CON or become poisoned for 1 hour.  
17	Success, Save CON or suffer one level of exhaustion.  
18	Failure, but the scroll is not destroyed.  
19	Failure, roll on the *Wild magic table*.  
20	Failure. Suffer one level of exhaustion.  
21	Failure. The scroll withers and shrivels, caustic smoke rises. You and anyone within 30 ft. save CON or suffer 1d8 poison damage per spell level. Critical failure = *poisoned* for 1 minute.  
22	Failure. You sink to the floor and fall asleep deeply for 8 hours. *(Can be woken up with an action, extremely loud noise (such as the *Shatter* spell), any damage*)  
23	Failure, the scroll bursts into flame in your hands, 1d6 fire damage per spell level, no save.  
24	Failure. Become deafened for 1 hour.  
25	Failure, Save CON, suffer 1d12 necrotic damage per spell level or half on success.  
26	Failure. The GM chooses a result from this table.  
27	Failure. Roll twice on this table, ignore this result on the second time.  
28	Failure. You and anyone within 60 ft. save DEX or be struck by lightning for 1d10 lightning damage per spell level.  
29	Failure, but the GM tells you a thematically apropriate side-effect of the spell that happens in addition to all its other effects. Re-roll if no idea.  
30	Failure. Suffer 2 levels of exhaustion.  
31	Failure. You and anyone within 120 ft. save WIS vs. 1d12 radiant damage. Critical failure = Also be blinded for 10 minutes.  
32	Failure. Become blinded for 1 hour.  
33	Failure. Become paralyzed for 1 minute.  
34	Failure. Become poisoned for 1 hour.  
35	Failure. Become deafened for 1 day.  
36	Failure. Become petrified for 1 day.  
37	Failure. Suffer 3 levels of exhaustion.  
38	Failure. Drop to 0 hit points. You are *dying*.  
39	Failure. You are subjected to *Power word: Kill*.  
