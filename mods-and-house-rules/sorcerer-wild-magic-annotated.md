# Sorcerer: Wild Magic (House Rule)

## Explanation

The two features **Tides of Chaos** and **Wild Magic Surge** require the permission of the Dungeon Master to be used.  
This is the core feature of the class, and it can be denied by the DM, either through malice, fear or forgetfulness.

I perceive the design intent of these two features as such:

- The player should not have fine control over when a surge happens
- Tides of Chaos gives the player a powerful tool (advantage on demand), but they might be punished by their own wild magic.
- Randomness is fun.

The problems I see in these features are these:

- The surges do not happen very often. This is most likely a result of a campaign with few combat encounters and few rests. Not many spells are cast, thus not many surges happen.
- The fact that both DM and player forget that the feature exists, exacerbates the first problem.
- The DM can deny the ability at a crucial moment, which should not be possible for a core feature of a subclass.
- These two features are stark deviations from the rest of the class rules. No other feature requires express permission from the GM.

The intent of these rules changes is to

- Remove the DM out of the equation. This brings the Wild Magic Surge feature in line with the rest of the Player´s Handbook.
- Make more surges happen.

## Rules

Changes to the original text are in *italics*.

### Wild Magic Surge

Starting when you choose this origin at 1st level, your spellcasting can unleash surges of untamed magic.   
Immediately after you cast a sorcerer spell of 1st level or higher, *roll a d20*.

*If your roll is equal to or lower than the spell slot level*, roll on the **Wild Magic Surge** table to create a random magical effect.

### Tides of Chaos

Starting at 1st level, you can manipulate the forces of chance and chaos to gain advantage on one attack roll, ability check, or saving throw. Once you do so, you must finish a long rest before you can use this feature again.  
*Whenever you suffer a **Wild Magic Surge**, you regain the use of this feature.*
