# Mod: Custom Dnd Weapons and properties

This is a list of modded and/or house-ruled weapons for DnD 5e.  
Some of these rules are taken from the core rules and elaborated upon.

## Weapons

| Name | Cost | Damage | Weight | Properties |
| --- | ---| --- | ---| ---|
| Acid Vial | 25 gp/vial | 2d6 acid | 1 lb/vial | Ammunition, thrown (range 20/40), bundle [^1] |
| Holy water | 25 gp/vial | 2d6 radiant vs undead/fiends | 1 lb/vial | Ammunition, thrown (range 20/40), bundle [^1] |


## Weapon property: Bundle

This property can only be used with thrown weapons that carry a payload (oil flasks, acid vials, etc.).  
When multiple pieces of ammunition are affixed together, they can be thrown in the same attack.  
The damage increases by **one damage die** of the same type for each extra piece of ammunition. [^2]

Depending on the quality of the bundle, it might break apart during flight. (E.g. badly improvised or fixed with bad materials).  
Excessively large quantities might be too heavy to throw, in this case, the DM might impose a penalty to the range.

## Links

- [DnD Weapon List](https://www.dndbeyond.com/sources/phb/equipment#Weapons)
- [DnD Weapon Properties](https://www.dndbeyond.com/sources/phb/equipment#WeaponProperties)

# Footnotes

[^1]: Bundle: New weapon property, explained in this document
[^2]: Example: Kriv binds together 4 acid vials with leather straps. The base damage for one vial is 2d6 acid, so he has a single-use weapon that deals 5d6 acid damage on impact.