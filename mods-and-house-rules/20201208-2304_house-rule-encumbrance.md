# House rule: Simple encumbrance

Instead of tracking every single minor item and pound of weight a character carries, we are only interesting in *heavy* and *bulky* things.

For anything more, you can get still get creative (Build sleds, rent wagons, rent carriers, use spells etc.).

## Rules: Weight

This rule is about the *weight* of things you carry.

You can carry only a certain number of item that are *heavy* (Examples see below).  
This number is **1 plus your STR modifier**

## Rules: Bulk

This rule is about the *size* of things you carry.

A *Medium*-sized creature[^2] can carry the following:

- One Medium-sized item on their back
- One Medium-sized item in their backpack
- One Medium-sized item with both hands
- Heavy armor such as *plate mail* is worn on the body, it does not take up a slot.

A *Medium*-sized object is about the size of an adult human. [^1]

## Examples

- An *anvil* is heavy.
- A *Medium* sized person is 2x heavy.
- A suit of heavy armor is heavy.
- 2500 coins of any kind are heavy.
- 50 pounds of material are heavy. (E.g. 10 gold bars)

# Footnotes

[^1]: See *Player\`s Handbook*, page 191 for sizes
[^2]: Larger characters, such as *ogres* or *giants* can carry more.