# House rule: Cleaving attacks

When a melee attack reduces a creature to 0 hit points, any excess damage from that attack might carry over to another creature nearby. The attacker targets another creature within reach and, if the original attack roll can hit it, applies any remaining damage to it.  
Repeat this process until no damage remains or there are no more targets within reach.