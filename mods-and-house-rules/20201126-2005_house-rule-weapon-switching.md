# House rule: weapon switching

Any character can use the following ability:

**Swap weapons.** Use your *bonus action* to swap your active weapons.

## Clarifications
 
- This does not replace the regular rules from the *Player\`s Handbook*, you can still use your one *free object interaction* to draw a weapon.

## Exceptions

- *Shields* are strapped to your arm. You can not swamp them with your bonus action or free object interaction.

## Comments

- This weakens *feats* such as *Dual Wielder*. This is intentional, the weapon swapping rules cause confusion.