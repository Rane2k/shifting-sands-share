# Wild Magic Surge

Starting when you choose this origin at 1st level, your spellcasting can unleash surges of untamed magic.  
Immediately after you cast a sorcerer spell of 1st level or higher, roll a d20.

If your roll is equal to or lower than the spell slot level, roll on the Wild Magic Surge table to create a random magical effect.

# Tides of Chaos

Starting at 1st level, you can manipulate the forces of chance and chaos to gain advantage on one attack roll, ability check, or saving throw. Once you do so, you must finish a long rest before you can use this feature again.  
Whenever you suffer a **Wild Magic Surge**, you regain the use of this feature.

