<div hidden>
20201121-1735_consumable-flaming-arrow
tags: #item #consumable #arrow #flaming-arrow #fire #fire-damage
</div>

# Arrow: Flaming arrow

When fired with any bow, this arrow deals an additional 1d6 fire damage on impact. The attack counts as a magical weapon attack.
Regardless of hit or miss, the arrow dissipates after the attack, it´s magical energy is spent completely.

## Rules summary

**Flaming arrow (1/arrow).** 1d6 fire damage plus your regular bow damage. 
