<div hidden>
20201121-1739_consumable-arrow-of-slaying-green-dragons
tags: #item #consumable #arrow #arrow-of-slaying #dragon #green-dragon
</div>

# Arrow: Arrow of Slaying - Green dragons

An arrow of slaying is a magic weapon meant to slay a particular kind of creature. Some are more focused than others; for example, there are both arrows of dragon slaying and arrows of blue dragon slaying. If a creature belonging to the type, race, or group associated with an arrow of slaying takes damage from the arrow, the creature must make a DC 17 Constitution saving throw, taking an extra 6d10 piercing damage on a failed save, or half as much extra damage on a successful one.

Once an arrow of slaying deals its extra damage to a creature, it becomes a nonmagical arrow.

Other types of magic ammunition of this kind exist, such as bolts of slaying meant for a crossbow, though arrows are most common.

This particular arrow is meant for *green dragons*.

## Rules summary

**Slay (1/arrow).** On a hit against a *green dragon* with this arrow, the victim makes a DC 17 CON aving throw, taking **6d10 piercing damage** or half as much on a successful save.  
All other damage and effects from the hit remain unaffected (normal bow damage, etc.).
