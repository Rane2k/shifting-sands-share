# Night spider poison

Night spider poison is an *injury* poison.

- Xd8 poison damage
- CON save DC Y, half damage on success
- If the poison damage reduces the target to 0 hit points, the target is stable but poisoned for 10 minutes, even after regaining hit points, and is paralyzed while poisoned in this way.

## DCs

- Harvest DC
	- Broodmother: 16
	- Night spider: 13
	- Spiderling: 11
- Refining DC
	- Broodmother: 13
	- Night spider/Spiderling 12

## Quality

- Supreme: DC 13 6d8 (Broodmother only)
- High: DC 13 4d8 
- Medium: DC 12 3d8
- Low: DC 11 2d8
- Poor: DC 10 1d8