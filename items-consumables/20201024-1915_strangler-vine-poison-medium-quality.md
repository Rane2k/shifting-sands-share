20201024-1915_strangler-vine-poison-medium-quality

# Dose of strangler vine poison

Value: 300 GP
Weight: 0.1lb
Doses: 1

## Effect

A creature subjected to this poison must make a DC 13 CON saving throw against 2d6 poison damage.
On a successful save, they take half damage.
On a failed save, they take the full damage and must repeat the saving throw at the end of each of their turns and take an additional 1d6 poison damage on a failed save. On a success, the effect ends.

On a critical failure on any of the rolls, they also suffer the *poisoned* condition for 1 hour. 

## Usage

Inhaled. This is a powder that takes effect when inhaled. Blowing the powder subjects creatures in a 5-foot cube to its effect. The resulting cloud dissipates immediately afterward. Holding one´s breath is ineffective against this poison, as it affects nasal membranes, tear ducts and other parts of the body.

## Harvest

This poison can be harvested from a dead or incapacitated strangler vine or assassin-vine. The risk of accidentally getting the pollen into your eyes and nose is high.

*This poison has been extracted from the pollen of the deadly strangler vine plants, which entangle their victims within their vines and disable them with their pollen dust.*