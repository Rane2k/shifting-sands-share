# Earthblood plate armor

*This ornate platemail bears the glyph of clan **Earthblood** on it\`s left chest, artfully etched into the metal. On the right chest, three **ruby fragments** are inset, the sockets forming a triangle.*

## Plate armor

This item is a suit of *plate armor* (AC 18, requires STR 15, weight 65lb., disadvantage on *Stealth*).

## Gemstone sockets

The three **gemstones** set into the chest can be expended to produce a spell-like effect, depending on the type of gemstone used. Currently, three rubies are set into the armor, but it looks like other gemstones could be used.
A replacement gemstone must have a worth of **at least 500 GP**.

Any spells requiring attacks or saving throws use the following scores:

- Spell attack bonus: **+7**
- Spell save DC: **15**

### Ruby

You can **use your action** to expend one ruby from the armor to cast one of the following spells:

- *Scorching Ray* at 4th level.
- *Fire Shield* at 4th level.