
# Short sword: Silver avenger

You gain +1 to attack rolls with this *silvered* weapon.

Any hit against a fiend deals an additional 1d6 of slashing damage.

Once per day, while wielding this sword, you can use your action and expend one of your hit dice to cast spare the dying at one target.


## Rules summary

**Silver avenger**. +1 to hit. 1d6 slashing (+1d6 vs. *fiends*), *light*, *finesse*

**Spare the dying (1/day)**. Use your *action* to cast *spare the dying*