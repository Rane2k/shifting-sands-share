# Adamantine maul: Stonebreaker

You gain **+1** to attack rolls with this *adamantine* weapon.

Any hit of this weapon against a solid object (not a creature) is an automatic critical hit.
Any hit against a construct deals an additional 1d6 of bludgeoning damage.

It can produce the *Shatter*[^Shatter] spell (See summary for details)

## Rules summary

**Stonebreaker.** +1 to hit. 2d6 bludgeoning (+1d6 vs. *constructs*), *heavy*, *two-handed*

**Charges.** 5 charges, regain 1d4 charges each dawn. Can never go above 5.

**Shatter.** Use an **action** to cast *Shatter* at level 2, 3, 4 or 5 (your choice) without using material components.
The save DC is **15**. For each **1** or **8** on the damage roll, the maul loses 1 charge.
If the spell consumes more charges than are left, roll a d20 for each. On a **1**, the maul loses this ability.

## Lore

> This maul is forged from Adamantium. It has been created to crush rock and other hard materials to powder, while also being a potent close combat weapon.  
> Runes are engraved on the hilt, among them: "Thunder", "Shatter", "Transience", "Volatile"
> Mauls like these are sometime carried by dwarven foremen. It can cause a pinpoint thunder eruption to quickly clear out piles of debris.

# Links

[^Shatter]: [_Shatter_ @ DndBeyond](https://www.dndbeyond.com/spells/shatter)