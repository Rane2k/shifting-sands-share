# Night spider web

This is a piece of web that was cut from the lair of a night spider brood.

It is in the material plane during the night, and vanishes into the ethereal realm during the day.
*Iron* reduces this property.

## Usages

The usage of this fabric is up to you. It can be fashioned into clothing, bags and other things.
