# Greatsword: Dragonbane

You gain a +1 bonus to attack rolls made with this *enchanted* weapon.

This weapon has been enchanted to specifically slay dragons and their kind.
Any attack against a dragon or a being of draconic descent, such as dragonborne critically hits on a roll of 19 or 20.
Additionally, the weapon deals an additional 1d6 force damage when hitting such a creature.

Wielding this sword grants advantage against a dragon's Frightful Presence ability.

*The dwarven runes for "Dragon", "dragonyouth" and "Bane" are engraved into the blade´s fuller.*

## Rules summary

**Dragonbane**. +1 to hit. 2d6 slashing (+1d6 vs. draconic beings). *Critical hit* on **19 or 20** vs. draconic beings. *Heavy*, *two-handed*

**Face the terror.** Wielding this sword makes you immune to any dragon\`s *Frightful Presence* ability.