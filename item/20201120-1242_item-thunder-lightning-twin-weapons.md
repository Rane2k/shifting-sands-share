# Twin weapons: Thunder & Lightning

This is a pair bonded weapons, a hammer and an axe.  
While wielding both of them, you gain +1 to attack rolls with these *elemental* weapons.

"Thunder" is a *light hammer* that deals additional thunder damage.  
"Lighting" is a *handaxe* that deals additional lightning damage.

They both have the *thrown* and *light* properties, and can be used for *Two-weapon-fighting* (**bonus action** for an extra attack).

## Rules summary

**Thunder.** +1 to hit. 1d4 bludgeoning, 1d4 thunder, *light*, *thrown*
**Lightning.** +1 to hit. 1d4 slashing, 1d4 lightning, *light*, *thrown*

**Recall.** While wielding one of them, you can your **bonus action** to summon the other to your hand. (Requires *attunement*, range 300 ft.)