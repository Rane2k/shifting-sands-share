# Dragonleather mask

*A mask Kriv has commisioned in Razal Kut. It is made from the wing leather of a slain foe, the green dragon Zuaco.*

## Rules summary

**Mask +1.** While wearing this, you gain **+1** to your armor class.

**Made to measure.** Requires *attunement* by a dragonborne.[^1]

**Menacing aspect.** While wearing this, you get advantage on *Intimidate* checks against anyone with **6 or fewer hit dice**, when threatening violence.

# Links / Footnotes

[^1]: [DnD Basic Rules: *Attunement* @ DndBeyond](https://www.dndbeyond.com/sources/basic-rules/magic-items#Attunement)
