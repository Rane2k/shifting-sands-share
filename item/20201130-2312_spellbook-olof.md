# Olof´s Spellbook

## Description

> A wizard\s spellbook. On the inside, the name **Olof Geijer** is written in meticulous handwriting.  
> The last few pages hold a few general notes about fighting dragons:  
>  
> - Red dragons are impervious to fire
> - Green dragons usually live in swamps, herbs and potions which protect from toxins are highly advised when fighting these.
> - A dragon\`s scales are often as thick and protective as the plates of a knight\s armor. They seem to be even denser on older specimens.
> - Do not battle a dragon inside its lair. Dragons are intelligent and will use their home terrain to their advantage.

## Mechanics

**Spellbook.** Any of the pages in this book can be used like a *spell scroll*. A *wizard* can transcribe them to their own spellbook at the normal cost.  
The following spells are contained within these pages:

- 1st level: *detect magic, mage armor, magic missile, shield*
- 2nd level: *misty step, suggestion*
- 3rd level: *counterspell, fireball, fly*
- 4th level: *greater invisibility, ice storm*
- 5th level: *cone of cold*