# Weapon: Giant spear

This spear is made for *Huge* creatures.  

Similar to weapons with the *Heavy* property, any creature that is *Large* has disadvantage on attack rolls made with this weapon.  
Any creature that is *Medium* or smaller can not wield the weapon at all.

The weight, damage and range of this weapon are larger than other spears, all other properties are the same.

The damage dealt by this weapon is 3d6 (3d8 if used with two hands), all other properties are the same as a regular spear.

*While humanoid-sized spears are made of branches, this spear is made from the tree trunk itself. The tip is sharpened and hardened with fire.*

## Rules summary

**Giant spear.** 3d6 bludgeoning (3d8 two-handed), range 10 feet, *Heavy-3*, *versatile*, *reach*, *thrown (range 40/120)*

**Property: Heavy-3.** Creatures that are *Large* have disadvantage on attack rolls with heavy-3 weapons. A heavy-3 weapon's size and bulk make it too cumbersome for a *Large* or smaller creature to use effectively.