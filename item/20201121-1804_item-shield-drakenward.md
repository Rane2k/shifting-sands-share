# Shield: Drakenward

While holding this shield, you have a +1 bonus to AC.
This bonus is in addition to the shield's normal bonus to AC.

You may use your reaction to interpose this shield between you and a dragon's *breath weapon* or a similar effect.  
You and any creature in a 15-foot cone behind you have advantage against the breath weapon's effect.

## Rules summary

**Shield +1**. While wielding this you have a total of **+3** to your armor class.

**Protection.**. You can use your **reaction** to protect yourself and anyone in a 15-foot cone behind you against a directed cone-attack, such as a dragon\`s breath.