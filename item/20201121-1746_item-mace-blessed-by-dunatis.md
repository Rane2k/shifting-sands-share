# Mace blessed by Dunatis

You gain a +1 bonus to attack rolls made with this *holy* weapon.

An attuned character can use this weapon as a *Holy Symbol of Dunatis*, the god of mountains and peaks,  while wielding it.
They also have advantage on any Survival checks pertaining to surviving in a mountain environment, such as climbing.
In addition to all other stats of a *mace*, this weapon is *versatile*.

*A well-balanced mace. Along the shaft, dwarven and giant runes are inscribed, roughly translatable to "Heart of the mountain, grant us the strength to endure."*

## Rules summary

**Holy mace**. +1 to hit. 1d6 bludgeoning, *versatile*

**Mountain survival (Requires attunement)**. Advantage on *Survival* checks for surviving in the mountains.

**Holy Symbol of Dunatis (Requires attunement)**. Counts as a holy symbol of the god Dunatis.