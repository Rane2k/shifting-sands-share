20210207-1728_lore-adamantine-glows-purple-in-magic-light
tags: #adamantine #lore #purple #light #magic #magic-light #light-spell #continual-flame #daylight #dancing-lights

# Lore: Adamantine emits a light purple glow when subjected to magical light

Adamantine, the hardest known metallic substance, faintly glows in a purple color when subjected to light from a magical source.[^1]

This is of course not the primary purpose of the material. Using it merely for illuminating a space would be a crass display of wealth.
However, it helps when proving if any quantity of it is genuine, making it very hard to make counterfeit adamantine bars or other products, such as protective gear.

# Footnotes

[^1]: Such as the *Light*, *Continual Flame*, *Dancing Lights* and *Daylight* spells.