<div hidden>
20201121-1619_dwarven-clan-names.md
tags: #dwarves #names #dwarven-names #tradition #clan-name #naming-tradition #naming-custom #obsidian-forge #silver-mine #golden-cup #silver-brew
</div>

# Dwarven naming customs: Clan name

- Dwarves do not name themselves after their gods. It is presumptous.
- Dwarf clans are large, they usually are named after a thing they constructed (obsidian forge), are known to inhabit (silvermine) or produce (golden cup, silverbrew)

# Connection

- [Family names](./../../zk/20201024-2234_on-dwarven-naming-traditions.md)
- [Name pool](./../living-documents/20201121-1537_name-pool-dwarves.md)