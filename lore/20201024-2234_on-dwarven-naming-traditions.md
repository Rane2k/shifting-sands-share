<div hidden>
20201024-2234_on-dwarven-naming-traditions
tags: #dwarves #names #dwarven-names #frostpeak #damren-ironhilt #ironhilt #dimforge #tradition #family-name #naming-tradition #naming-custom #clan-name
</div>

# Dwarven naming customs: Family name and clan name

The *Frostpeak* clan and some others in the same region have begun taking family names in addition to their clan-names.

It is unclear to outsiders why they do this. Names such as "*Damren Ironhilt* of clan *Frostpeak*" sometimes confuse people.

These family names can sometimes grow to clan names, when a new settlement is established or a clan consists of a majority of dwarves from the same clan.  
In both cases, often new family names arise after a generation or so.