20201215-2042_group-goal-recruit-allies-vs-dragons-and-shadow
tags: #goal #group #group-goal #player-goal #allies #recruit allies #shadow #dragon

# Group goal: Recruit allies vs. dragons and darkness

Recruit allies for our fight against the darkness and/or the dragons.

## Worked on

- 1 Urgond?
- 2 Udragg?
- 3 Chorsoth?
- 4 Goblins?