20201230-1520_group-goal-bring-snog-and-trix-out-alive
tags: #goal #group #group-goal #player-goal #goblins #trix #snog #metm #s32 #s31 #s30 #s29 #s28 #s27

# Group goal: Bring Snog and Trix back home alive and well.

*(as the title says)*

This goal is #solved #completed #finished

## Reward

- Level 7&8: Hard, group goal = x2, 5 players and 2 goblins = total divided by 6 = 6400 * 2 / 6
		- **2133 XP/player**