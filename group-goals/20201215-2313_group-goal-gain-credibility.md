20201215-2313_group-goal-gain-credibility.md
tags: #goal #group #group-goal #player-goal #renown #credibility #fame

# Group goal: Gain credibility

> Gain fame/renown, so that we can be recognized in the world (Gain credibility)

## Progress

- #s33 Sorrow spreads information about the #shadow-hunters
	- Persuasion 21
	- Persuasion 18
	- Persuasion 16
- #s32 Agorn mounts #zuacos-skull on the skiff