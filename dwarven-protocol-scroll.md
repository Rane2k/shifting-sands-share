# Protocol of the Earthblood council. Session #10697

## Attendants

- Lawmaker Grangurn Earthblood
- Forgemaster Hormun Brassbuckle
- Lawspeaker Dafim Ironhorn
- Forgepriest Gryden Earthblood
- Forgepriest Tygrom Mountainblood
- Trademaker Nartryn Ironhorn
- Armsmaster Glefrin Earthblood
- Lawspeaker Thurbrek Mountainblood  
  
- Keeper of protocol: Hjulrak Earthblood

## Matters at hand

- The matter of the unscheduled council meeting
- Judgement: Selfish water distribution of the middle houses
- On reinforcing the western hallways
- Establising of a new mineshaft near the east gate.
- The matter of the orc prisoner
- Judgement: The black elf
- On dangers from below
- The matter of the broken seal

# On the matter of the unscheduled council meeting

- Several of the lower housings complain about this unscheduled session. Their duties are derelicted, they demand to know why.
- It is explained to them that this council sitting is of the utmost importance, due to the matter of the broken seal.

# Judgement: Selfish water distribution of the middle houses

*A somewhat boring account of a kind of trial, in which some members of the middle housings unfairly took more water than they deserved when it was their turn on water distribution duty.*

# On reinforcing the western hallways

*Disgusting plant juice has dribbled over the pages, this part is almost unreadable*.

# Establising of a new mineshaft near the east gate.

*A lengthy and technical discussion on opening a new mineshaft near the gate on the east side of the mountain. The scroll goes into great detail on the risks and advantages, as well as estimations on how much iron ore can be extracted in the next 27 weeks. One of the mentioned risks is "agitating the grick nests"*

# The matter of the orc prisoner

- The orc prisoner has become more aggressive. He is a nuisance to anyone passing by the jail.
- Keeping him any longer disturbs the peace of our settlement.
- Guarding him is an onerous and unthankful duty.
- The decision was made to let him go. When the next caravan arrives, he will be sent along with them to find his way back to his tribe.

# Judgement: The black elf

*Unfortunately, a large ink blot obscures most of this section and the next one, only the heading and a few partial words near the edges are readable. The glyphs for ".. in our backs." and ".. predicament.." can be read. The last word seems to be ".. danger".*

# The matter of the broken seal

- All must give up their reserves of gemstones.
- A great many voices were raised after this proclamation. Lawspeaker Earthblood made it absolutely clear that this is an extremely unusual, but ultimately necessary sacrifice.
- More protection runes must be crafted, to erect a new gate as soon as possible.
- The trademaster has agreed to send more people from the surface to aid with the reconstruction of the gate.
- Armsmaster Earthblood has tripled the guard shifts. Emergency weapons training is being issued to all who are not busy with crafting runes or restoring the gate.