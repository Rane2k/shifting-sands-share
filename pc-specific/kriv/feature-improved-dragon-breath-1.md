# Improved Dragon Breath (Tier 1)

## Description

Your dragon breath grows more potent, the longer you walk the path of true dragons.

- Increase the damage die of your **dragon breath** to a d8.
- Increase the range of your **dragon breath** by 5ft.

## Prerequisites

- You must be a **dragonborn** (or similar creature with a draconic heritage or breath weapon).