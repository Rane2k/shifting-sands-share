# Feature: Hunting Claws

Kriv can now use his claws as melee weapons.[^Ferocity]

They have the following features:

- The **finesse** and **light** properties
- They have a range of *5 feet*
- They deal *1d4 slashing damage* plus *1d6 poison damage*[^Deadly weapons]at a range of *5 feet*
- Their attacks count as *melee weapon attacks* and *unarmed attacks*
- They can be used as climbing gear.

Kriv can use them in the following ways:

- he is *proficient* with these weapons
- he can use a **bonus action** to make one attack with a claw, unless both your hands are full.

*Kriv awakens his feral hunter's instinct. His claws grow to dagger-like proportions and poison drips from them.*

## Connection

[^Ferocity]: Granted by choosing *Ferocity*, while offering a *Roper* skull to *Malar*, god of the hunt.
[^Deadly weapons]: Granted by choosing *Deadly weapons* while offering *Zuaco\`s skull* to *Malar*, god of the hunt.