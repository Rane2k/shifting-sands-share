# Gildas and her siblings

| # | Name | Cause of death | significant event? | notes |
| --- |--- | --- | --- | --- |
| 1 | Illuro | Slain by an ogre | #battle-of-langford-creek | Oldest brother|
| 2 | Darrel | Burnt to death by a *fireball* spell. | #battle-at-the-summer-palace | Officer |
| 3 | ? | | | |
| 4 | ? | | | |
| 5 | Gilda | Alive!| | |
| 6 | Golond | Poisoned in his sleep in the castle.| Last possible heir to the old king **Thelos**, his death means **Torgil** become the new king. | Gildas twin brother | 
| 7 | ? | | | |
| 8 | Jared | ? | | Youngest brother, Battle wizard in training. Had to go to war despite his youth, because of the need. Pulled out of university. |

## Illuro

Slain by an ogre. Gilda witnessed this.

## Golond

Gildas twin brother. Poisoned in his sleep.

Notes: **Tharain** was the actual heir to the throne, the crown prince, he also was killed, by a poisoned arrow during a royal hunt. This was murder as well.

## Darrel

Officer in King Thelos\` army, died in a *fireball* during the siege of the *summer palace*, a pivotal battle in the #hornburg-civil-war

## Jared

Youngest brother, Battle wizard in training. Had to go to war despite his youth, because of the needs of the war.
He was pulled out of university for this.

# Questions for Elli? OLD

- Was Gilda present at the death of each brother?
- Please invent the death and name of **one** brother, #3, #4 or #7
- On the deaths
	- Did all of Gilda´s brother die of violent means? (or maybe of sickness, starvation, etc.?)
	- Were all of them adults already? 
	- Did all of them die during the war (directly or indirectly?)
	- Were all of them soldiers or did some perform other functions in the war or even at home? (medics, battle wizards, maybe one of the older one was an officer?)

## Notes / Answers

- All of the brother were adults when the war took them.
- All of them died in the war, directly or indirectly.

## GM note on the questions

- Please do not invent all of the brothers at once, so that we have some imagination space left for later. Names are okay, tough. :-)
