# Feat: Spirit realm magic (tier 1)

## Mechanics

### Spellcasting

You gain the *Spellcasting* ability. 
You gain **one 1st level spell slot**, which you can use to cast spells you know. When you do this, it is expended, and you regain it after a *long rest*.

Your spellcasting ability for this is WIS.  

Your spell save DC is 8 + your proficiency bonus + your WIS modifier.  
Your spell attack bonus is your proficiency bonus + your WIS modifier. 

### Cantrip

You know *two* cantrips of your choice from the following list:

- [Guidance](https://www.dndbeyond.com/spells/guidance)
- [Message](https://www.dndbeyond.com/spells/message)
- [Minor illusion](https://www.dndbeyond.com/spells/minor-illusion)
- [Druidcraft](https://www.dndbeyond.com/spells/druidcraft)
- [Spare the dying](https://www.dndbeyond.com/spells/spare-the-dying)

Cantrips do not require spellslots.

### Spell list

Your magic stems from the deep connection to the spirit realm.  
When you receive this feat, choose one of the following 1st level spells:

- [Identify](https://www.dndbeyond.com/spells/identify)
- [Faerie Fire](https://www.dndbeyond.com/spells/faerie-fire)
- [Comprehend Languages](https://www.dndbeyond.com/spells/comprehend-languages)
- [Dissonant Whispers](https://www.dndbeyond.com/spells/dissonant-whispers)
- [Heroism](https://www.dndbeyond.com/spells/heroism)
- [Sleep](https://www.dndbeyond.com/spells/sleep)

Whichever spell you take, when you use it, it´s effect is produced through you by the spirit realm, the realm of dreams, visions and memory.

# Character sheet

Note these changes on your character sheet, on the *Spellcasting* page:

- The two cantrips you have chosen.
- The spell you have chosen.
- The *spellcasting ability*, *spell save DC* and *spell attack bonus*

# Links

- [Player´s Handbook: _Spellcasting rules_](https://www.dndbeyond.com/compendium/rules/basic-rules/spellcasting)