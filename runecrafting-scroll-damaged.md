# Runecrafting: Anchoring magic within our work

*By Engrinn of clan onyx goblet*

## Introduction

*A drawn-out introduction fills the first few parchment pages.*

## The basic formula

You absolutely need the following things:

- A *vessel*, the thing onto which the magic is imprinted.
- An *anchor* which binds the magic to the vessel.
- A *command*, which describes the magical effect that is produced.
- A *source* of magical power, the spark that brings the work to life.

### Vessel

The *vessel* it the thing that is being enchanted. It is important to remind ourselves that this is not limited to the basic form of things, such as worked stone or forged metal.
It could theoretically be anything. A person or animal, a landscape feature such as a mountain or lake, anything you can conceive of.

It has to be said tought, that defined, clear forms such as shaped stone or metal is the simplest material to work with.

### Anchor

As discussed in **On the laws of permanence of magic**, magic is generally unstable, often volatile. Measures can be taken to bind the arcane forces to the material.

In the following chapters, we discuss the effect glyphs, or runes can be used to moor the magic to the material, on the example of stone, a simple material to work with.

It is to be said that other forms of anchors can be imagined, such as inscriptions, elaborate paintings and other phantastical concepts. We focus on the simple elegance of runes engraved onto stone.

*A lengthy description of the carving and applying of runes to flat stone surfaces follows.*

## Command

We do not simply want to bind magic within the vessel - tough that is also sometimes useful, again, I recommend you study **On the laws of permanence of magic** - we want the magic to do something, perform a task.

The command is the description of the task out object is made to fulfil. An everglowing lamp is made to provide light. A shield is made to protect it´s wielder.
The possibilities are endless, but the more complicated the task, the more complicated the command.

The easiest commands are those which have been researched and recorded by clerics, wizards and other artisans in the form of prayer scrolls or in the pages of a spellbook. *Light*, *Floating Disk* or *Shield* are examples of well-researched magical formulae.

There is of course the possibility to make either simpler formulae - for example a direct release of magical force - or more specific ones, such as protection from elemental fire or a specific type of beast.

*This description goes on and on, the gist is that the only limits are the knowledge and creativity of the artisan.*

## Source

This is the crucial element. We can forge the mightiest suits of armor or erect the boldest towers, inscribe them with words of power and command them to do our bidding, but without a connection to the weave of magic, our efforts are as nothing.

Any work that is to be imbued with arcane power needs a source of this power.

There are two common ways to achieve this:

- Infusing the work with magical power for prolonged amounts of time.
- Using an inherently magical material for the vessel, the anchor or both

*It follows a long and meandering discussion of magical exposure, which boils down to "**On the laws of permanence of magic**" and "expose the work to magical force every day"*

## Runes

### Light

The simplest of magics, even easier to control than fire. Every aspiring runecrafter should start with this.

*There is a picture of the **Light** rune.*

### Lightning

Elemental force, associated with storms and (incorrectly) thunder.

*There is a picture of the **Lightning** rune.*

### Dragon

*There is a picture of the rune signifying **Dragon**.*

*The rest of the scroll was mangled in whatever catastrophe befell the dwarves at the gate.*